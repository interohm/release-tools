# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::AutoDeploy::Start do
  subject(:task) { described_class.new(product_version: product_version) }

  let(:product_version) do
    build(
      :product_version,
      releases: build(
        :releases_metadata,
        omnibus_gitlab_ee: build(
          :component_metadata,
          ref: '14.0.202105311735+ac7ff9aa2f2.2ab4d081326'
        )
      )
    )
  end

  after do
    FileUtils.rm_r('deploy_vars.env') if File.exist?('deploy_vars.env')
  end

  describe '#new' do
    context 'when using the default constructor' do
      it 'fetches the last auto deploy' do
        expect(ReleaseTools::ProductVersion)
          .to receive(:last_auto_deploy)

        described_class.new
      end
    end
  end

  describe '#execute' do
    before do
      allow(task).to receive(:store_deploy_version)
      allow(task).to receive(:notify)
      allow(task).to receive(:sentry_release)
    end

    it 'tracks a Sentry release' do
      tracker = stub_const('ReleaseTools::Deployments::SentryTracker', spy)
      gitlab_sha = product_version[ReleaseTools::Project::GitlabEe].sha

      expect(task).to receive(:sentry_release).and_call_original

      without_dry_run do
        task.execute
      end

      expect(tracker).to have_received(:new)
      expect(tracker).to have_received(:release).with(gitlab_sha)
    end

    it 'stores deploy_version in an env variable' do
      result = 'DEPLOY_VERSION=14.0.202105311735-ac7ff9aa2f2.2ab4d081326'
      expect(task).to receive(:store_deploy_version).and_call_original

      without_dry_run do
        task.execute
      end

      expect(File.read('deploy_vars.env')).to eq(result)
    end

    it 'notify on slack' do
      omnibus_ref = product_version[ReleaseTools::Project::OmnibusGitlab].ref
      deploy_version = ReleaseTools::AutoDeploy::Version.new(omnibus_ref).to_package
      gitlab_branch = product_version[ReleaseTools::Project::GitlabEe].ref

      notifier = stub_const('ReleaseTools::Slack::CoordinatedPipelineTagNotification', spy)

      expect(task).to receive(:notify).and_call_original

      without_dry_run do
        task.execute
      end

      expect(notifier).to have_received(:new)
        .with(
          deploy_version: deploy_version,
          auto_deploy_branch: gitlab_branch,
          tag_version: product_version
        )
      expect(notifier).to have_received(:execute)
    end

    context 'in dry-run' do
      it 'does not tracks a Sentry release' do
        tracker = stub_const('ReleaseTools::Deployments::SentryTracker', spy)

        expect(task).to receive(:sentry_release).and_call_original

        task.execute

        expect(tracker).not_to have_received(:new)
        expect(tracker).not_to have_received(:release)
      end

      it 'does not produce an environment file' do
        expect(task).to receive(:store_deploy_version).and_call_original

        task.execute

        expect(File).not_to exist('deploy_vars.env')
      end

      it 'does not notify on slack' do
        notifier = stub_const('ReleaseTools::Slack::CoordinatedPipelineTagNotification', spy)

        expect(task).to receive(:notify).and_call_original

        task.execute

        expect(notifier).not_to have_received(:new)
        expect(notifier).not_to have_received(:execute)
      end
    end
  end
end
