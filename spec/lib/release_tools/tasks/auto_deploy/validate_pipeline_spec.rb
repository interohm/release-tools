# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::AutoDeploy::ValidatePipeline do
  subject(:task) { described_class.new(deploy_version) }

  let(:deploy_version) { '14.0.202105311735+ac7ff9aa2f2.2ab4d081326' }
  let(:product_version) do
    build(
      :product_version,
      releases: build(
        :releases_metadata,
        omnibus_gitlab_ee: build(
          :component_metadata,
          ref: deploy_version
        )
      )
    )
  end

  describe '#execute' do
    let(:gitlab_project) { ReleaseTools::Project::GitlabEe }
    let(:gitlab_sha) { product_version[gitlab_project].sha }
    let(:gitlab_branch) { product_version[gitlab_project].ref }
    let(:passing_build) { instance_double(ReleaseTools::PassingBuild) }
    let(:metrics_client) { instance_double(ReleaseTools::Metrics::Client) }
    let(:passing_build_result) { true }

    before do
      allow(ReleaseTools::ProductVersion)
        .to receive(:from_package_version)
        .with(deploy_version)
        .and_return(product_version)

      allow(ReleaseTools::PassingBuild).to receive(:new)
        .with(gitlab_branch, gitlab_project)
        .and_return(passing_build)
      allow(passing_build).to receive(:success_for_auto_deploy_rollout?)
        .with(gitlab_sha)
        .and_return(passing_build_result)

      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(metrics_client)
    end

    context 'when a passing build is found' do
      let(:passing_build_result) { true }

      it 'tracks a success pipeline' do
        expect(metrics_client).to receive(:inc)
          .with('auto_deploy_gitlab_pipeline_total', { labels: 'success' })

        expect { task.execute }.not_to raise_error(SystemExit)
      end
    end

    context 'when a passing build is not found' do
      let(:passing_build_result) { false }

      it 'tracks a failed pipeline and raise SystemExit' do
        expect(metrics_client).to receive(:inc)
          .with('auto_deploy_gitlab_pipeline_total', { labels: 'failed' })

        expect { task.execute }.to raise_error(SystemExit)
      end

      context 'when SKIP_VALIDATE_PIPELINE is set' do
        around do |ex|
          ClimateControl.modify(SKIP_VALIDATE_PIPELINE: '1', &ex)
        end

        it 'tracks a failed pipeline and does not fail' do
          expect(metrics_client).to receive(:inc)
            .with('auto_deploy_gitlab_pipeline_total', { labels: 'failed' })

          expect { task.execute }.not_to raise_error(SystemExit)
        end
      end
    end

    context 'when using the default constructor' do
      subject(:task) { described_class.new }

      let(:deploy_version) { 'a package version' }

      it 'fetches deploy version from the ENV' do
        expect(ReleaseTools::ProductVersion)
          .to receive(:from_package_version)
          .with(deploy_version)
          .and_return(product_version)

        expect(metrics_client).to receive(:inc)
          .with('auto_deploy_gitlab_pipeline_total', { labels: 'success' })

        ClimateControl.modify(DEPLOY_VERSION: deploy_version) do
          task.execute
        end
      end
    end
  end
end
