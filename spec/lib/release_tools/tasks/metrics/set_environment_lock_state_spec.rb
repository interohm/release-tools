# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Metrics::SetEnvironmentLockState do
  subject(:service) { described_class.new(environment, lock_reason) }

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  let(:environment) { 'gstg-cny' }
  let(:lock_reason) { 'locked_qa' }
  let(:env) { 'gstg' }
  let(:stage) { 'cny' }

  let(:all_reasons) do
    %w[locked_deployment locked_qa locked_deployment_failed locked_qa_failed
       locked_post_deploy_migration locked_post_deploy_migration_failed]
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)
    end

    it 'sets metric' do
      expect(delivery_metrics)
        .to receive(:set)
        .with(
          'auto_deploy_lock_state',
          1,
          { labels: "locked_qa,#{env},#{stage}" }
        )

      (all_reasons - ['locked_qa']).each do |lock_reason|
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_lock_state',
            0,
            { labels: "#{lock_reason},#{env},#{stage}" }
          )
      end

      without_dry_run { service.execute }
    end

    context 'with nil lock_reason' do
      let(:lock_reason) { nil }

      it 'unsets all metric labels for given environment' do
        all_reasons.each do |reason|
          expect(delivery_metrics)
            .to receive(:set)
            .with(
              'auto_deploy_lock_state',
              0,
              { labels: "#{reason},#{env},#{stage}" }
            )
        end

        without_dry_run { service.execute }
      end
    end

    context 'when in dry_run mode' do
      it 'does not set metrics' do
        expect(delivery_metrics).not_to receive(:set)

        service.execute
      end
    end
  end
end
