# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::ReleaseEnvironment::QaNotifier do
  subject(:notifier) do
    described_class.new(pipeline_id: pipeline_id,
                        environment_name: environment_name,
                        release_environment_version: release_environment_version)
  end

  let(:pipeline_id) { '123' }
  let(:environment_name) { '16-10-stable' }
  let(:release_environment_version) { '16-10-stable-9dce3c3d' }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notification) { stub_const('ReleaseTools::Slack::ReleaseEnvironment::QaNotification', spy) }

  let(:job) do
    create(
      :job,
      name: 'release-environments-qa'
    )
  end

  before do
    allow(client)
      .to receive(:pipeline_job_by_name)
      .and_return(job)

    allow(ReleaseTools::Slack::ReleaseEnvironment::QaNotification)
      .to receive(:new)
      .and_return(instance_double(ReleaseTools::Slack::ReleaseEnvironment::QaNotification, execute: nil))
  end

  describe '#execute' do
    it 'sends a slack notification' do
      expect(notification)
        .to receive(:new)
        .with({ job: job, environment_name: environment_name, release_environment_version: release_environment_version })

      notifier.execute
    end

    context 'when the job is not found' do
      let(:job) { nil }

      it 'does not send a slack notification' do
        expect(notification).not_to receive(:new)

        notifier.execute
      end
    end
  end
end
