# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::GrafanaAnnotator do
  subject(:annotator) { described_class.new(blocker) }

  let(:blocker_no_duration_label) do
    ReleaseTools::Deployments::BlockerIssue.new(
      create(:issue, labels: ['release-blocker'], created_at: "2023-12-05T04:29:15.929Z", id: 100)
    )
  end

  let(:blocker_one_duration_label) do
    ReleaseTools::Deployments::BlockerIssue.new(
      create(
        :issue,
        id: 101,
        labels: ['release-blocker', 'Deploys-blocked-gstg::6hr'],
        created_at: "2023-12-05T04:29:15.929Z"
      )
    )
  end

  let(:blocker_two_duration_label) do
    ReleaseTools::Deployments::BlockerIssue.new(
      create(
        :issue,
        id: 102,
        labels: ['release-blocker', 'Deploys-blocked-gstg::6hr', 'Deploys-blocked-gprd::8hr'],
        created_at: "2023-12-05T04:29:15.929Z"
      )
    )
  end

  let(:blocker_root_cause_label) do
    ReleaseTools::Deployments::BlockerIssue.new(
      create(
        :issue,
        id: 103,
        labels: [
          'release-blocker', 'Deploys-blocked-gstg::6hr', 'Deploys-blocked-gprd::8hr', 'RootCause::Flaky-Test'
        ],
        created_at: "2023-12-05T04:29:15.929Z"
      )
    )
  end

  before do
    enable_feature(:grafana_annotations)

    stub_request(:post, "https://example.com/api/annotations").to_return(body: 'foo', status: 200)
  end

  around do |ex|
    ClimateControl.modify('GRAFANA_URL' => 'https://example.com') do
      ex.run
    end
  end

  describe '#execute' do
    let(:blocker) { blocker_no_duration_label }

    it 'generates the right Grafana request with the correct payload' do
      expect(JSON)
        .to receive(:generate)
        .with(
          "dashboardUID" => "delivery-auto_deploy_packages",
          "panelId" => 2,
          "time" => 1_701_750_555_000,
          "timeEnd" => 1_701_750_555_000,
          "text" => "<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://example.com/foo/bar/-/issues/100\">A title for issue 100</a>",
          "tags" => []
        )

      annotator.execute
    end

    context 'with one duration label' do
      let(:blocker) { blocker_one_duration_label }

      it 'generates the right Grafana request for blocker with one duration label' do
        expect(JSON)
          .to receive(:generate)
          .with(
            "dashboardUID" => "delivery-auto_deploy_packages",
            "panelId" => 2,
            "time" => 1_701_750_555_000,
            "timeEnd" => 1_701_772_155_000,
            "text" => "<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://example.com/foo/bar/-/issues/101\">A title for issue 101</a>",
            "tags" => []
          )

        annotator.execute
      end
    end

    context 'with two duration labels' do
      let(:blocker) { blocker_two_duration_label }

      it 'generates the right Grafana request for blocker with two duration label' do
        expect(JSON)
          .to receive(:generate)
          .with(
            "dashboardUID" => "delivery-auto_deploy_packages",
            "panelId" => 2,
            "time" => 1_701_750_555_000,
            "timeEnd" => 1_701_779_355_000,
            "text" => "<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://example.com/foo/bar/-/issues/102\">A title for issue 102</a>",
            "tags" => []
          )

        annotator.execute
      end
    end

    context 'with root cause label' do
      let(:blocker) { blocker_root_cause_label }

      it 'generates the right Grafana request for blocker with root cause label' do
        expect(JSON)
          .to receive(:generate)
          .with(
            "dashboardUID" => "delivery-auto_deploy_packages",
            "panelId" => 2,
            "time" => 1_701_750_555_000,
            "timeEnd" => 1_701_779_355_000,
            "text" => "<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://example.com/foo/bar/-/issues/103\">A title for issue 103</a>",
            "tags" => ["RootCause-Flaky-Test"]
          )

        described_class.new(blocker_root_cause_label).execute
      end
    end
  end
end
