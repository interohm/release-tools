# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Fetcher do
  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:bridge_pipeline1) { create(:bridge_job, :failed, name: 'qa:smoke-main:gstg-cny') }
  let(:bridge_pipeline2) { create(:bridge_job, :failed, name: 'qa:smoke:gstg-cny') }
  let(:bridge_pipeline3) { create(:bridge_job, :failed, name: 'foo') }

  subject(:fetcher) { described_class.new(environment: 'gstg-cny', pipeline_id: 123) }

  before do
    allow(client)
      .to receive_messages(pipeline_bridges: [bridge_pipeline1, bridge_pipeline2, bridge_pipeline3], pipelines: create(:pipeline))
  end

  describe '#execute' do
    it 'returns quality pipelines' do
      expect(fetcher.execute.count).to eq(2)
    end

    it 'returns instances of Quality::Pipeline' do
      fetcher.execute do |pipeline|
        expect(pipeline)
          .to be_a(Releasetools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Pipeline)
      end
    end
  end
end
