# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::AutoDeployPackageState do
  subject(:service) { described_class.new }

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  let(:product_version) do
    build(
      :product_version,
      auto_deploy_ref: version,
      releases: build(
        :releases_metadata,
        cng_ee: build(:component_metadata, ref: cng_version),
        omnibus_gitlab_ee: build(:component_metadata, ref: omnibus_version)
      )
    )
  end

  let(:version) { '16.10.202402200600-f54d5e31aa4.5527995deba' }
  let(:cng_version) { '16.10.202402200600+f54d5e31aa4' }
  let(:omnibus_version) { '16.10.202402200600+f54d5e31aa4.5527995deba' }

  let(:omnibus_pipelines) { [build(:pipeline, :running)] }
  let(:cng_pipelines) { [build(:pipeline, :running)] }

  around do |tests|
    ClimateControl.modify(AUTO_DEPLOY_TAG: version, &tests)
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)

      allow(ReleaseTools::ProductVersion)
        .to receive(:new)
        .with(version)
        .and_return(product_version)

      allow(ReleaseTools::GitlabDevClient)
        .to receive(:pipelines)
        .with(
          ReleaseTools::Project::OmnibusGitlab,
          hash_including({ ref: omnibus_version, order_by: 'id', sort: 'desc' })
        )
        .and_return(omnibus_pipelines)

      allow(ReleaseTools::GitlabDevClient)
        .to receive(:pipelines)
        .with(
          ReleaseTools::Project::CNGImage,
          hash_including({ ref: cng_version, order_by: 'id', sort: 'desc' })
        )
        .and_return(cng_pipelines)

      %w[missing pending building ready failed].each do |state|
        allow(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            0,
            { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},#{state},#{version}" }
          )

        allow(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            0,
            { labels: "#{ReleaseTools::Project::CNGImage.dev_path},#{state},#{version}" }
          )
      end
    end

    it 'sets metric for both packager pipelines' do
      expect(ReleaseTools::GitlabDevClient)
        .to receive(:pipelines)
        .with(
          ReleaseTools::Project::OmnibusGitlab,
          hash_including({ ref: omnibus_version, order_by: 'id', sort: 'desc' })
        )

      expect(ReleaseTools::GitlabDevClient)
        .to receive(:pipelines)
        .with(
          ReleaseTools::Project::CNGImage,
          hash_including({ ref: cng_version, order_by: 'id', sort: 'desc' })
        )

      expect(delivery_metrics)
        .to receive(:set)
        .with(
          'auto_deploy_package_state',
          1,
          { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},building,#{version}" }
        )

      expect(delivery_metrics)
        .to receive(:set)
        .with(
          'auto_deploy_package_state',
          1,
          { labels: "#{ReleaseTools::Project::CNGImage.dev_path},building,#{version}" }
        )

      %w[missing pending ready failed].each do |state|
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            0,
            { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},#{state},#{version}" }
          )

        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            0,
            { labels: "#{ReleaseTools::Project::CNGImage.dev_path},#{state},#{version}" }
          )
      end

      without_dry_run { service.execute }
    end

    context 'when pipeline has succeeded' do
      let(:omnibus_pipelines) { [build(:pipeline, :success)] }
      let(:cng_pipelines) { [build(:pipeline, :manual)] }

      it 'sets metric for both packager pipelines' do
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},ready,#{version}" }
          )

        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::CNGImage.dev_path},ready,#{version}" }
          )

        without_dry_run { service.execute }
      end
    end

    context 'when pipeline has failed' do
      let(:omnibus_pipelines) { [build(:pipeline, :failed)] }
      let(:cng_pipelines) { [build(:pipeline, :canceled)] }

      it 'sets metric for both packager pipelines' do
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},failed,#{version}" }
          )

        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::CNGImage.dev_path},failed,#{version}" }
          )

        without_dry_run { service.execute }
      end
    end

    context 'when pipeline is missing' do
      let(:omnibus_pipelines) { [] }
      let(:cng_pipelines) { [build(:pipeline, :waiting_for_resource)] }

      it 'sets metric for both packager pipelines' do
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::OmnibusGitlab.dev_path},missing,#{version}" }
          )

        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_package_state',
            1,
            { labels: "#{ReleaseTools::Project::CNGImage.dev_path},pending,#{version}" }
          )

        without_dry_run { service.execute }
      end
    end

    context 'when in dry_run mode' do
      it 'does not set metrics' do
        expect(ReleaseTools::GitlabDevClient).to receive(:pipelines).twice
        expect(delivery_metrics).not_to receive(:set)

        service.execute
      end
    end
  end
end
