# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::EnvironmentStateTransition do
  include_context 'metric registry'

  let(:service) { described_class.new(environment, stage, new_state) }
  let(:environment) { 'gstg' }
  let(:stage) { 'cny' }
  let(:new_state) { 'ready' }

  let(:query_string) { "auto_deploy_environment_state{target_env=\"gstg\",target_stage=\"cny\"}" }

  let(:query_spy) { instance_spy(ReleaseTools::Prometheus::Query) }

  let(:ready_state_metric_value) { '0' }
  let(:locked_state_metric_value) { '1' }

  let(:results) do
    [
      {
        "metric" => {
          "__name__" => "auto_deploy_environment_state",
          "target_env" => "gstg",
          "target_stage" => "cny",
          "env_state" => 'ready'
        },
        "value" => [1_435_781_451.781, ready_state_metric_value]
      },
      {
        "metric" => {
          "__name__" => "auto_deploy_environment_state",
          "target_env" => "gstg",
          "target_stage" => "cny",
          "env_state" => 'locked'
        },
        "value" => [1_435_781_451.781, locked_state_metric_value]
      }
    ]
  end

  let(:response) do
    {
      "status" => "success",
      "data" => {
        "resultType" => "vector",
        "result" => results
      }
    }
  end

  before do
    allow(ReleaseTools::Prometheus::Query).to receive(:new).and_return(query_spy)

    allow(query_spy).to receive(:run).with(query_string).and_return(response)
  end

  describe '#current_state' do
    subject(:current_state) { service.current_state }

    before do
      # Call valid_transition? so that current_state is set
      service.valid?
    end

    context 'with non nil current state' do
      it 'returns current_state' do
        expect(current_state).to eq('locked')
      end
    end

    context 'with nil state' do
      let(:locked_state_metric_value) { '0' }

      it 'returns nil' do
        expect(current_state).to be_nil
      end
    end
  end

  describe '#valid_transition?' do
    subject(:valid?) { service.valid? }

    where(:locked_state_metric_value, :ready_state_metric_value, :baking_time_metric_value, :awaiting_promotion, :new_state, :validity) do
      [
        ['0', '0', '0', '0', 'ready', true],
        ['0', '0', '0', '0', 'locked', true],
        ['1', '0', '0', '0', 'ready', true],
        ['1', '0', '0', '0', 'locked', true],
        ['0', '1', '0', '0', 'locked', true],
        ['0', '1', '0', '0', 'ready', false],
        ['1', '0', '0', '0', 'baking_time', true],
        ['0', '1', '0', '0', 'baking_time', false],
        ['0', '0', '1', '0', 'ready', true],
        ['0', '1', '0', '0', 'awaiting_promotion', true],
        ['1', '0', '0', '0', 'awaiting_promotion', false]
      ]
    end

    with_them do
      it "returns validity" do
        expect(valid?).to eq(validity)
      end
    end
  end
end
