# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::AutoDeployPressure do
  include_context 'metric registry'

  subject(:service) { described_class.new }

  let(:client) do
    stub_const(
      'ReleaseTools::GitlabClient',
      class_spy(
        ReleaseTools::GitlabClient,
        commits: []
      )
    )
  end

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  before do
    allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)
  end

  describe '#execute' do
    def stub_versions(result)
      query_spy = spy(rails_version_by_role: result)

      stub_const('ReleaseTools::Prometheus::Query', query_spy)
    end

    it 'performs one comparison per unique SHA' do
      stub_versions({ 'gstg-cny' => 'aaa', 'gstg' => 'aaa', 'gprd-cny' => 'aaa', 'gprd' => 'aaa' })

      expect(client).to receive(:compare).once

      %w[gstg-cny gstg gprd-cny gprd].each do |label|
        expect(delivery_metrics).to receive(:set).with(described_class::METRIC, 0, { labels: label })
      end

      service.execute
    end

    it 'includes the pressure from the previous role in its total' do
      stub_versions({ 'gstg-cny' => 'aaa', 'gstg' => 'bbb', 'gprd-cny' => 'ccc', 'gprd' => 'ddd' })

      expect(service).to receive(:commit_pressure).with('aaa', 'master')
        .and_return(5)
      expect(service).to receive(:commit_pressure).with('bbb', 'aaa')
        .and_return(3)
      expect(service).to receive(:commit_pressure).with('ccc', 'bbb')
        .and_return(1)
      expect(service).to receive(:commit_pressure).with('ddd', 'ccc')
        .and_return(2)

      # 5 commits from `master` to `aaa`
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 5, { labels: 'gstg-cny' })

      # 3 commits from `bbb` to `aaa` + 5 from previous
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 8, { labels: 'gstg' })

      # 1 commit from `ccc` to `bbb` + 8 from previous
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 9, { labels: 'gprd-cny' })

      # 2 commit from `ddd` to `ccc` + 9 from previous
      expect(delivery_metrics).to receive(:set)
        .with(described_class::METRIC, 11, { labels: 'gprd' })

      service.execute
    end

    it "doesn't compare unknown versions" do
      stub_versions({ 'gstg' => '', 'gprd-cny' => '' })

      expect(client).not_to receive(:compare)

      %w[gstg-cny gstg gprd-cny gprd].each do |label|
        expect(delivery_metrics).to receive(:set).with(described_class::METRIC, 0, { labels: label })
      end

      service.execute
    end
  end
end
