# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Client do
  let(:client) { described_class.new }

  it_behaves_like 'security_client #open_security_merge_requests'
  it_behaves_like 'security_client #release_tools_bot'
  it_behaves_like 'security_client #latest_merge_request_pipeline'
  it_behaves_like 'security_client #method_missing'
  it_behaves_like 'security_client #respond_to?'

  describe '#cancel_merge_when_pipeline_succeeds' do
    let(:merge_request) { build(:merge_request) }

    it 'calls the class method on GitlabClient' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:cancel_merge_when_pipeline_succeeds)
        .with(merge_request)

      client.cancel_merge_when_pipeline_succeeds(merge_request)
    end
  end
end
