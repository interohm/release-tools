# frozen_string_literal: true

require 'rake_helper'

describe 'quality tasks', :rake do
  around do |ex|
    ClimateControl.modify(
      CI_PIPELINE_ID: '12345',
      DEPLOY_ENVIRONMENT: 'gstg-cny',
      DEPLOY_VERSION: 'abc',
      &ex
    )
  end

  describe 'notify', task: 'quality:notify' do
    it 'creates a QA notification' do
      expect(ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Notifier).to receive(:new).with(
        pipeline_id: '12345',
        deploy_version: 'abc',
        environment: 'gstg-cny'
      ).and_return(instance_spy(ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Notifier))

      task.invoke
    end
  end

  describe 'report_failure', task: 'quality:report_failure' do
    it 'Creates a Quality Failure report' do
      expect(ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::QualityFailures).to receive(:new).with(
        environment: 'gstg-cny',
        coordinated_pipeline_id: '12345'
      ).and_return(instance_spy(ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::QualityFailures))

      task.invoke
    end
  end

  describe 'update_report', task: 'quality:update_report' do
    it 'updates an existing report' do
      expect(ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Triager).to receive(:new).with(
        environment: 'gstg-cny',
        deploy_version: 'abc'
      ).and_return(instance_spy(ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Triager))

      task.invoke
    end
  end
end
