# frozen_string_literal: true

# This ensures we don't push to the repo during tests
ENV['TEST'] = 'true'

# Don't let production feature flags affect a test run
ENV.delete('FEATURE_INSTANCE_ID')

# Stub API tokens
ENV['RELEASE_BOT_DEV_TOKEN'] = 'test'
ENV['RELEASE_BOT_OPS_TOKEN'] = 'test'
ENV['RELEASE_BOT_PRODUCTION_TOKEN'] = 'test'
ENV['RELEASE_BOT_VERSION_TOKEN'] = 'test'

# SimpleCov needs to be loaded before everything else
require_relative 'support/simplecov'

require_relative '../lib/release_tools'
require 'active_support/core_ext/string/strip'
require 'active_support/core_ext/object/inclusion'

require 'rspec-parameterized'

Dir[File.expand_path('support/**/*.rb', __dir__)].sort.each { |f| require f } # rubocop:disable Lint/RedundantDirGlobSort

# make sure user configuration does not affect tests
fake_home = ENV["HOME"] = File.expand_path("../support/testdata/home", __dir__)
Rugged::Settings['search_path_global'] = fake_home
Rugged::Settings['search_path_system'] = '/dev/null'
Rugged::Settings['search_path_xdg'] = '/dev/null'

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.filter_run :focus
  config.order = 'random'

  unless ENV['CI']
    config.example_status_persistence_file_path = './spec/examples.txt'
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.around do |ex|
    # HACK (rspeicher): Work around a transient timing failure when the user has
    # a Git template dir (such as ~/.git_template)
    ClimateControl.modify(GIT_TEMPLATE_DIR: '') do
      ex.run
    end
  end
end
