# frozen_string_literal: true

module ReleaseTools
  module Security
    module ComponentBranchHelper
      def generate_link_list(projects, versions)
        projects.each do |project|
          branches = versions.map { |version| version.stable_branch(ee: project.ee_branch?) }

          branches.prepend(project.default_branch).each do |branch|
            link = "https://gitlab.com/#{project.path}/-/commits/#{branch}"

            yield(project.metadata_project_name, branch, link)
          end
        end
      end
    end
  end
end
