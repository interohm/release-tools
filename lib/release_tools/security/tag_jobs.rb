# frozen_string_literal: true

module Security
  class TagJobs
    def initialize(versions)
      @versions = versions
    end

    def generate
      base_config.merge(stages).merge(jobs).to_yaml
    end

    private

    def base_config
      {
        '.with-bundle' => {
          'before_script' => [
            'bundle install --jobs=$(nproc) --retry=3 --quiet'
          ]
        }
      }
    end

    def stages
      {
        'stages' => @versions.map { |version| "security_release:tag:#{version}" }
      }
    end

    def job(version)
      {
        'image' => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
        'stage' => "security_release:tag:#{version}",
        'script' => [
          "bundle exec rake security:tag[#{version}]"
        ],
        'extends' => '.with-bundle'
      }
    end

    def jobs
      @versions.each_with_object({}) do |version, hash|
        hash["security_release_tag:#{version}"] = job(version)
      end
    end
  end
end
