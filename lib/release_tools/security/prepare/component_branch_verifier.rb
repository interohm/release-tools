# frozen_string_literal: true

module ReleaseTools
  module Security
    module Prepare
      class ComponentBranchVerifier
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::ComponentBranchHelper

        FailedComponentError = Class.new(StandardError)
        CouldNotCompleteError = Class.new(StandardError)

        # Maximum commit depth to review in each branch.
        # Higher values might indicate false positives on stable branches
        COMMIT_DEPTH = 20

        def initialize
          @projects = ReleaseTools::ManagedVersioning::PROJECTS
          @versions = ReleaseTools::Versions.next_versions
          @statuses = {}
        end

        def execute
          if project_statuses.all?('success')
            logger.info('All components are green! \u{2705}')
            send_slack_notification(:success)
          else
            logger.info(log_failure_message)

            raise FailedComponentError
          end
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotCompleteError
        end

        private

        attr_reader :projects, :versions, :statuses

        def project_statuses
          [].tap do |status_list|
            projects.each do |project|
              branches_for_project(project).each do |branch|
                status = ReleaseTools::Services::ComponentStatusService.new(
                  project: project,
                  branch: branch,
                  commit_depth: COMMIT_DEPTH
                ).execute

                statuses[project.metadata_project_name] = {} unless statuses[project.metadata_project_name]
                statuses[project.metadata_project_name][branch] = status
                status_list << status
              end
            end
          end
        end

        def branches_for_project(project)
          versions.map do |version|
            version.stable_branch(ee: project.ee_branch?)
          end.prepend(project.default_branch)
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Component status',
            status: status,
            release_type: :patch
          ).send_notification
        end

        def error_message
          [].tap do |message|
            message << "Component status check failed. If this job continues to fail, the status of the projects can be checked manually:\n"
            generate_link_list(projects, versions) do |project, branch, link|
              message << "- #{project} - #{branch}: #{link}"
            end
          end.join("\n")
        end

        def log_failure_message
          [].tap do |message|
            message << "\n\n\u{274C} The following components do not have green pipelines:\n"
            statuses.each do |project_name, branches|
              project = projects.find do |p|
                p.metadata_project_name == project_name
              end

              branches.each do |branch, status|
                next if status == 'success'

                message << "- #{project.metadata_project_name} - #{branch} - #{status}: https://gitlab.com/#{project.path}/-/commits/#{branch}"
              end
            end
          end.join("\n")
        end
      end
    end
  end
end
