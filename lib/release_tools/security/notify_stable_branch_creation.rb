# frozen_string_literal: true

module ReleaseTools
  module Security
    # Send a  notification to security implementation issues that a new stable branch has been created,
    # so that developers can create a backport MR targeting the stable branch.
    class NotifyStableBranchCreation
      include ::SemanticLogger::Loggable

      # @param issue_crawler [Security::IssueCrawler] instance of the IssueCrawler class
      # @param project [Class] one of the classes in ReleaseTools::Project module
      # @param stable_branch_name [String] name of the new stable branch
      def initialize(issue_crawler, project, stable_branch_name)
        @issue_crawler = issue_crawler
        @project = project
        @stable_branch_name = stable_branch_name
      end

      def execute
        logger.info('Posting notification to security issues about creation of stable branch', stable_branch_name: stable_branch_name, project: project)

        issue_crawler.notifiable_security_issues_for(project).each do |implementation_issue|
          if mr_to_stable_branch_name(implementation_issue).present?
            logger.info('Security issue already has backport MR to stable branch', issue: implementation_issue.web_url)
            next
          end

          if existing_note(implementation_issue).present?
            logger.info('Security issue has already been notified about stable branch', issue: implementation_issue.web_url)
            next
          end

          post_notification(implementation_issue.issue)
        end
      end

      private

      attr_reader :issue_crawler, :project, :stable_branch_name

      def mr_to_stable_branch_name(implementation_issue)
        implementation_issue.backports.detect { |mr| mr.target_branch == stable_branch_name }
      end

      def post_notification(issue)
        logger.info('Posting stable branch creation notification to issue', issue: issue.web_url, stable_branch_name: stable_branch_name)

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.create_issue_note(
            issue.project_id,
            issue: issue,
            body: note_body(issue)
          )
        end
      end

      def note_body(issue)
        <<~EOF.squish
          #{usernames_string(issue)} #{note_prefix}. To be included in
          #{issue_crawler.security_tracking_issue.web_url}, please create a backport targeting the stable branch,
          following the security MR template. If this backport is not needed, please
          notify the `@gitlab-org/release/managers` and apply the ~"reduced backports" label
          to this issue. **When this issue is ready, apply the ~"security-target" label for it
          to be processed and linked to the upcoming patch release.**
        EOF
      end

      def note_prefix
        "The stable branch `#{stable_branch_name}` has been created"
      end

      def existing_note(issue)
        Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.issue_notes(issue.project_id, issue_iid: issue.iid).detect do |note|
            note.author.username == ReleaseTools::Bot::USERNAME && note.body.include?(note_prefix)
          end
        end
      end

      def usernames_string(issue)
        return "@#{issue.author.username}" if issue.assignees.empty?

        issue
          .assignees
          .map { |user| "@#{user.username}" }
          .join(', ')
      end
    end
  end
end
