# frozen_string_literal: true

module ReleaseTools
  module Services
    # Makes sure a merge request is automatically merged
    class AutoMergeService
      include ::SemanticLogger::Loggable

      PipelineNotReadyError = Class.new(StandardError)

      # we consider a pipeline ready only when in one of the following states
      PIPELINE_READY_STATES = %w[running pending].freeze

      attr_reader :merge_request, :commit

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      # @param commit [Gitlab::ObjectifiedHash] an optional commit object
      def initialize(merge_request, token:, commit: nil)
        @merge_request = merge_request
        @token = token
        @commit = commit
      end

      def execute
        # we need to wait for the pipeline to start or we cannot set merge_when_pipeline_succeeds
        wait_for_mr_pipeline_to_start

        merge_when_pipeline_succeeds
      end

      private

      attr_reader :token

      def wait_for_mr_pipeline_to_start
        logger.info('Waiting for a merge request pipeline')

        return if SharedStatus.dry_run?

        # pipeline for merge requests will be generated only after creating the merge request.
        # here we wait until the pipeline is created with an exponential back-off
        # In case of a high load, creating the pipeline may take more than 1 minute.
        # base_interval 5 and tries 10 will hit the 1 minute timeline on retry n 5 (in average)
        # and wait up to 6 minutes (in average) on the worst case.
        Retriable.with_context(:pipeline_created) do
          pipeline = latest_pipeline

          raise PipelineNotReadyError unless PIPELINE_READY_STATES.include?(pipeline&.status)

          logger.info('Merge request pipeline', pipeline: pipeline.web_url, status: pipeline.status.to_s)

          pipeline
        end
      end

      def latest_pipeline
        GitlabClient.pipelines(merge_request.project_id, ref: merge_pipeline_ref, per_page: 1).first
      end

      def merge_pipeline_ref
        "refs/merge-requests/#{merge_request.iid}/merge"
      end

      def merge_when_pipeline_succeeds
        logger
          .info('Accepting merge request', merge_request: merge_request.url)

        Services::MergeWhenPipelineSucceedsService
          .new(merge_request, token: token)
          .execute
      end
    end
  end
end
