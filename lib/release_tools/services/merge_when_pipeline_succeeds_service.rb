# frozen_string_literal: true

module ReleaseTools
  module Services
    # MergeWhenPipelineSucceedsService will approve a merge request and set it
    # to merge when pipeline succeeds
    class MergeWhenPipelineSucceedsService
      include ::SemanticLogger::Loggable

      PipelineNotReadyError = Class.new(StandardError)

      # we consider a pipeline ready only when in one of the following states
      PIPELINE_READY_STATES = %w[running pending].freeze

      attr_reader :client, :merge_request

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve and MWPS
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      # @param endpoint [string] the GitLab API endpoint
      def initialize(merge_request, token:, endpoint: GitlabClient::DEFAULT_GITLAB_API_ENDPOINT)
        @merge_request = merge_request
        @token = token
        @client = Gitlab::Client.new(endpoint: endpoint, private_token: token, httparty: GitlabClient.httparty_opts)
      end

      # execute starts the service operations
      #
      # This method is idempotent
      #
      # @raise [PipelineNotReadyError] when cannot find a ready pipeline for the merge request
      def execute
        Retriable.with_context(:api) do
          Services::ApproveService.new(merge_request, token: @token).execute

          merge_when_pipeline_succeeds
        end
      end

      def merge_when_pipeline_succeeds_options
        {
          merge_when_pipeline_succeeds: true,
          should_remove_source_branch: true
        }
      end

      def merge_when_pipeline_succeeds
        return if SharedStatus.dry_run?

        client.accept_merge_request(
          merge_request.project_id,
          merge_request.iid,
          **merge_when_pipeline_succeeds_options
        )
      end
    end
  end
end
