# frozen_string_literal: true

require 'release_tools/time_util'

module ReleaseTools
  module Slack
    class PreNotification
      include ::SemanticLogger::Loggable
      include ReleaseTools::TimeUtil
      include Utilities

      def initialize(deploy_version:, pipeline:)
        @deploy_version = deploy_version
        @pipeline = pipeline
      end

      def execute
        logger.info('Sending slack notification', deploy_version: deploy_version, pipeline: pipeline.web_url)

        params = {
          channel: ReleaseTools::Slack::ANNOUNCEMENTS,
          message: fallback_text,
          blocks: slack_block
        }

        ReleaseTools::Slack::Message.post(**params)
      end

      private

      attr_reader :deploy_version, :pipeline

      def fallback_text
        "Pre environment #{deployer_status} #{deploy_version}"
      end

      def slack_block
        blocks = ::Slack::BlockKit.blocks
        blocks.section { |block| block.mrkdwn(text: section_block) }

        blocks.context do |block|
          block.mrkdwn(text: clock_context_element[:text])
          block.mrkdwn(text: ":timer_clock: #{wall_duration}") if finished_or_failed?
        end

        blocks.as_json
      end

      def environment
        'pre'
      end

      def section_block
        [].tap do |text|
          text << environment_icon
          text << status_icon
          text << "*#{environment}*"
          text << "<#{pipeline.web_url}|#{deployer_status}>"
          text << "`#{deploy_version}`"
        end.join(' ')
      end

      def wall_duration
        duration(current_time - start_time).first
      end

      def start_time
        Time.parse(pipeline.created_at)
      end

      def finished_or_failed?
        [DEPLOYER_STATUS_SUCCESS, DEPLOYER_STATUS_FAILED].include?(deployer_status)
      end
    end
  end
end
