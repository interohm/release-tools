# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Security
      class AppSecNotifier
        include ::SemanticLogger::Loggable
        include ReleaseTools::Security::IssueHelper
        include Utilities

        # @param issuable [Issuable] => An issuable object that responds to URL
        # @param issue_type [string] => 'patch_blog_post', 'appsec_task_issue', or 'comms_security_task_issue'
        def initialize(issuable:, issue_type:)
          @issuable = issuable
          @issue_type = issue_type
        end

        def send_notification
          logger.info('Posting a message in the AppSec slack channel', issuable: issuable.url, issue_type: issue_type)

          ReleaseTools::Slack::Message.post(
            channel: ReleaseTools::Slack::SEC_APPSEC,
            message: fallback_message,
            blocks: slack_blocks
          )
        end

        private

        attr_reader :issuable, :issue_type

        def fallback_message
          "The #{issue_type.titleize} has been created: #{issuable.url}"
        end

        def slack_blocks
          blocks = ::Slack::BlockKit.blocks

          blocks.section { |section| section.mrkdwn(text: section_block) }
          blocks.context { |context| context.append(clock_context_element) }
          blocks.as_json
        end

        def section_block
          [].tap do |text|
            text << ':security-tanuki:'
            text << ':ci_passing:'
            text << section_text
          end.join(' ')
        end

        def section_text
          "*The #{api_link(issue_type.titleize, issuable.url)} for the #{api_link('upcoming patch release', security_tracking_issue.web_url)} has been successfully created.*"
        end
      end
    end
  end
end
