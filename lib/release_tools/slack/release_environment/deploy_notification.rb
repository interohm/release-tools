# frozen_string_literal: true

require 'release_tools/time_util'

module ReleaseTools
  module Slack
    module ReleaseEnvironment
      class DeployNotification
        include ::SemanticLogger::Loggable
        include ReleaseTools::TimeUtil
        include Utilities

        def initialize(pipeline:, environment_name:, release_environment_version:)
          @pipeline = pipeline
          @environment_name = environment_name
          @release_environment_version = release_environment_version
        end

        def execute
          params = {
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: fallback_text,
            blocks: slack_block
          }

          logger.info('Sending slack notification',
                      pipeline: pipeline.web_url,
                      environment_name: environment_name,
                      release_environment_version: release_environment_version,
                      slack_channel: params[:channel])

          ReleaseTools::Slack::Message.post(**params)
        end

        private

        attr_reader :release_environment_version, :environment_name, :pipeline

        def fallback_text
          "Release environment #{environment_name}: #{deployer_status} #{release_environment_version}"
        end

        def slack_block
          blocks = ::Slack::BlockKit.blocks
          blocks.section { |block| block.mrkdwn(text: section_block) }

          blocks.context do |block|
            block.mrkdwn(text: clock_context_element[:text])
            block.mrkdwn(text: ":timer_clock: #{wall_duration}") if finished_or_failed?
          end

          blocks.as_json
        end

        def section_block
          [].tap do |text|
            text << status_icon
            text << "Release Environment *#{environment_name}*"
            text << "<#{pipeline.web_url}|#{deployer_status}>"
            text << "`#{release_environment_version}`"
          end.join(' ')
        end

        def wall_duration
          duration(current_time - start_time).first
        end

        def start_time
          Time.parse(pipeline.created_at)
        end

        def finished_or_failed?
          [DEPLOYER_STATUS_SUCCESS, DEPLOYER_STATUS_FAILED].include?(deployer_status)
        end
      end
    end
  end
end
