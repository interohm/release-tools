# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Class used for setting the auto_deploy_environment_state metric
      class SetEnvironmentState
        include ::SemanticLogger::Loggable

        METRIC = 'auto_deploy_environment_state'
        STATES = %w[ready locked baking_time awaiting_promotion].freeze

        def initialize(environment, state)
          @client = ReleaseTools::Metrics::Client.new
          @environment = environment
          @state = state
        end

        def execute
          env, stage = parse_env
          environment_state_transition = ReleaseTools::Metrics::EnvironmentStateTransition.new(env, stage, state)

          unless environment_state_transition.valid?
            logger.warn('Cannot transition to desired state from current state', new_state: state, current_state: environment_state_transition.current_state, environment: environment)
            return
          end

          logger.info('Setting env_state', environment: environment, env: env, stage: stage, state: state)
          return if SharedStatus.dry_run?

          set_metric(state, env, stage)
        end

        private

        attr_reader :client, :environment, :state

        def set_metric(state, env, stage)
          client.set(METRIC, 1, labels: "#{state},#{env},#{stage}")

          (STATES - [state]).each do |other_state|
            client.set(METRIC, 0, labels: "#{other_state},#{env},#{stage}")
          end
        end

        def parse_env
          return [environment, 'main'] unless environment.end_with?('-cny')

          env = environment.delete_suffix('-cny')
          [env, 'cny']
        end
      end
    end
  end
end
