# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Components
      class UpdateGitaly
        include Helper
        include ::SemanticLogger::Loggable

        TARGET_PROJECT = Services::UpdateComponentService::TARGET_PROJECT
        ENV_TOKEN = 'GITLAB_BOT_PRODUCTION_TOKEN'
        TOKEN_NAME = 'release-tools approver bot'

        def initialize(token = nil)
          # Approving the merge request can't be done by the author, so for
          # approving and auto-merging we use gitlab-bot instead
          # of the release-tools bot.
          @gitlab_bot_token = token || ENV.fetch(ENV_TOKEN)
        end

        def execute
          if !changed?
            logger.info('Gitaly already up to date')
          elsif merge_request.exists?
            logger.info('Found existing merge request', merge_request: merge_request.url)

            notify_stale_merge_request if merge_request.notifiable?
          else
            logger.info('Creating merge request to update Gitaly')

            create_merge_request
          end

          rotate_token_if_needed
        end

        def notify_stale_merge_request
          logger.warn('Existing merge request is stale, issuing warning')

          return if SharedStatus.dry_run?

          Slack::AutoDeployNotification.on_stale_gitaly_merge_request(merge_request)

          merge_request.mark_as_stale
        end

        def create_merge_request
          ensure_source_branch_exists

          create_or_show_merge_request(merge_request)

          logger.info('Updating gitaly version')
          commit = Services::UpdateComponentService
            .new(Project::Gitaly, source_branch_name, skip_ci: true)
            .execute

          Services::AutoMergeService.new(merge_request, token: @gitlab_bot_token, commit: commit).execute
        end

        def source_branch_name
          'release-tools/update-gitaly'
        end

        def ensure_source_branch_exists
          logger.info('Making sure source branch exists', source_branch: source_branch_name)
          return if SharedStatus.dry_run?

          GitlabClient.find_or_create_branch(source_branch_name, TARGET_PROJECT.default_branch, TARGET_PROJECT)
        end

        def changed?
          Services::UpdateComponentService.new(Project::Gitaly, Project::Gitaly.default_branch).changed?
        end

        def merge_request
          @merge_request ||= UpdateGitalyMergeRequest.new(source_branch: source_branch_name)
        end

        def rotate_token_if_needed
          return if SharedStatus.dry_run?

          refresher = Services::ProjectAccessTokenRotator.new(TOKEN_NAME, project: Project::GitlabEe)
          refresher.execute

          return unless refresher.rotated?

          refresher.update_ci_var(ENV_TOKEN)
        end
      end
    end
  end
end
