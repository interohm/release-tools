# frozen_string_literal: true

module ReleaseTools
  module DockerHub
    class Verifier
      include ::SemanticLogger::Loggable

      def initialize(release_type:, version:)
        @client = ReleaseTools::DockerHub::Client
        @version = version
        @release_type = release_type
      end

      def execute
        logger.info('Checking Docker Hub tags with version number', release_type: release_type, version: version)

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          check_tag!("ee")
          check_tag!("ce")

          logger.info('Tags exist on Docker Hub')
        end

        send_slack_notification(:success)
      rescue StandardError => ex
        logger.fatal(error_message, error: ex)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :client, :version, :release_type

      def tag(repository)
        client.tag(version, repository: repository)
      end

      def check_tag!(repository)
        return if tag(repository).active?

        raise "Tag #{version}-#{repository}.0 does not exist on Docker Hub"
      end

      def error_message
        <<~MSG
          The tags for #{version} do not exist on Docker Hub yet, retry this job. If the failure persists, then check the publish job and make sure images are published on Docker Hub"
        MSG
      end

      def send_slack_notification(status)
        ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: "Verify docker tags for #{version}",
          status: status,
          release_type: release_type
        ).send_notification
      end
    end
  end
end
