# frozen_string_literal: true

module ReleaseTools
  module PublicRelease
    class UpgradeStop
      UPGRADE_PATH_PROJECT_ID = "278964" # https://gitlab.com/gitlab-org/gitlab
      UPGRADE_PATH_FILE = "config/upgrade_path.yml"

      def last_required_stop
        last_stop = upgrade_path_hash
          .map { |stop| Version.new("#{stop['major']}.#{stop['minor']}") }
          .select { |stop| stop <= latest_released_minor }
          .max

        last_stop.to_minor if last_stop
      end

      private

      def upgrade_path_hash
        response = Retriable.with_context(:api) do
          ReleaseTools::GitlabClient
            .get_file(UPGRADE_PATH_PROJECT_ID, UPGRADE_PATH_FILE, ReleaseTools::Project::GitlabEe.default_branch)
        end

        content = Base64.strict_decode64(response.content)
        YAML.safe_load(content)
      end

      def latest_released_minor
        @latest_released_minor ||=
          Retriable.with_context(:api) do
            Version.new(ReleaseTools::Versions.current_version.to_minor)
          end
      end
    end
  end
end
