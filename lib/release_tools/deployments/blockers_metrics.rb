# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockersMetrics
      include ::SemanticLogger::Loggable

      def initialize
        @client = Metrics::Client.new
        @fetcher = ReleaseTools::Deployments::BlockerIssueFetcher.new
      end

      def execute
        return unless Feature.enabled?(:deployment_blockers_metrics)

        @fetcher.fetch
        blockers = @fetcher.deployment_blockers

        calculator = ReleaseTools::Deployments::BlockersCalculator.new(blockers)
        blocker_types = calculator.failure_types

        blocker_types.each do |blocker, blocked_time|
          log_blocker_metric(blocker, blocked_time)
          create_metric_for(blocker, blocked_time)
        end
      end

      private

      attr_reader :client

      def week
        @fetcher.start_time.strftime('%Y-%m-%d')
      end

      def create_metric_for(blocker, blocked_time)
        root_cause = blocker.gsub(/[~"]/, '')
        hours_gprd_blocked = blocked_time[:gprd]
        hours_gstg_blocked = blocked_time[:gstg]
        blockers_per_category = blocked_time[:count]

        client.set(
          'deployment_blocker_count',
          blockers_per_category,
          labels: "#{root_cause},#{week}"
        )

        client.set(
          'deployment_hours_blocked',
          hours_gprd_blocked,
          labels: "#{root_cause},gprd,#{week}"
        )

        client.set(
          'deployment_hours_blocked',
          hours_gstg_blocked,
          labels: "#{root_cause},gstg,#{week}"
        )
      end

      def log_blocker_metric(blocker, blocked_time)
        logger.info(
          "Setting deployment blockers metric for #{blocker}",
          blockers_per_category: blocked_time[:count],
          hours_gprd_blocked: blocked_time[:gprd],
          hours_gstg_blocked: blocked_time[:gstg],
          labels: {
            root_cause: blocker.gsub(/[~"]/, ''),
            week: week
          }
        )
      end
    end
  end
end
