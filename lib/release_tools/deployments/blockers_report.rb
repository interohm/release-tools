# frozen_string_literal: true

require 'release_tools/issuable'
require 'release_tools/issue'
require_relative 'blockers_calculator'

module ReleaseTools
  module Deployments
    class BlockersReport < ReleaseTools::Issue
      def initialize
        @blockers = ReleaseTools::Deployments::BlockerIssueFetcher.new
      end

      def create
        client
          .create_issue(self, ReleaseTools::Project::Release::Tasks)
      end

      def title
        "Deployment Blockers - Week: #{start_time}-#{end_time}"
      end

      def description
        @blockers.fetch

        calculator = BlockersCalculator.new(deployment_blockers)

        variables = binding
        variables.local_variable_set(:start_time, start_time)
        variables.local_variable_set(:end_time, end_time)
        variables.local_variable_set(:staging_blocked_time, staging_deployment_blocked_time)
        variables.local_variable_set(:production_blocked_time, production_deployment_blocked_time)
        variables.local_variable_set(:deployment_blockers, deployment_blockers)
        variables.local_variable_set(:uncategorized_incidents, uncategorized_incidents)
        variables.local_variable_set(:failure_types, calculator.failure_types)
        variables.local_variable_set(:total_failure_type_gstg, calculator.total_failure_type_gstg)
        variables.local_variable_set(:total_failure_type_gprd, calculator.total_failure_type_gprd)

        ERB
          .new(template, trim_mode: '-')
          .result(variables)
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      def labels
        ''
      end

      private

      def start_time
        @blockers.start_time.strftime('%Y-%m-%d')
      end

      def end_time
        @blockers.end_time.strftime('%Y-%m-%d')
      end

      def production_deployment_blocked_time
        deployment_blockers.sum(&:hours_gprd_blocked)
      end

      def staging_deployment_blocked_time
        deployment_blockers.sum(&:hours_gstg_blocked)
      end

      def deployment_blockers
        @blockers.deployment_blockers
      end

      def uncategorized_incidents
        @blockers.uncategorized_incidents
      end

      def template
        template_path = File
          .expand_path('../../../templates/deployment_blockers_report.md.erb', __dir__)

        File.read(template_path)
      end

      def client
        ReleaseTools::GitlabClient
      end
    end
  end
end
