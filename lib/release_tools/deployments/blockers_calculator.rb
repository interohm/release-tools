# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockersCalculator
      def initialize(blockers)
        @blockers_with_failure_type = blockers.select { |issue| issue.root_cause.present? }
      end

      def failure_types
        @failure_types ||= {}.tap do |failure_types|
          @blockers_with_failure_type.each do |issue|
            root_cause = issue.root_cause

            next unless issue.with_deploys_blocked_label?

            failure_types[root_cause] ||= { gstg: 0, gprd: 0, count: 0 }
            failure_types[root_cause][:gstg] += issue.hours_gstg_blocked
            failure_types[root_cause][:gprd] += issue.hours_gprd_blocked
            failure_types[root_cause][:count] += 1
          end
        end
      end

      def total_failure_type_gstg
        failure_types.values.sum { |failure_type| failure_type[:gstg] }
      end

      def total_failure_type_gprd
        failure_types.values.sum { |failure_type| failure_type[:gprd] }
      end
    end
  end
end
