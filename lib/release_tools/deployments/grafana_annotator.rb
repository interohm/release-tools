# frozen_string_literal: true

require 'net/http'

module ReleaseTools
  module Deployments
    class GrafanaAnnotator
      include ::SemanticLogger::Loggable
      # Dashboard Uid and panel id are hardcoded to the "delivery: Auto-Deploy packages information" dashboard
      DASHBOARD_UID = "delivery-auto_deploy_packages"
      PANEL_ID = 2
      MILLISECONDS_PER_SECOND = 1_000

      def initialize(blocker)
        @blocker = blocker
      end

      def execute
        payload = generate_annotation_payload

        add_grafana_annotation(payload)
      end

      private

      attr_reader :blocker

      def generate_annotation_payload
        JSON.generate(
          "dashboardUID" => DASHBOARD_UID,
          "panelId" => PANEL_ID,
          "time" => blocker.created_at.to_i * MILLISECONDS_PER_SECOND,
          "timeEnd" => blocker.ended_at.to_i * MILLISECONDS_PER_SECOND,
          "text" => "<a target=\"_blank\" rel=\"noopener noreferrer\" href=\"#{blocker.data.web_url}\">#{blocker.data.title}</a>",
          "tags" => blocker.root_cause_label ? [blocker.root_cause_label.gsub('::', '-')] : []
        )
      end

      def add_grafana_annotation(annotation_payload)
        logger.info("Grafana URL: #{grafana_uri.host}")

        http = Net::HTTP.new(grafana_uri.host, grafana_uri.port)
        http.use_ssl = true

        request = generate_grafana_request(annotation_payload)
        response = http.request(request)

        logger.info "Adding annotation to Grafana: #{annotation_payload}"

        if response.code.to_i == 200
          logger.info("Annotation added successfully.")
        else
          logger.info("Failed to add annotation. Response code: #{response.code}")
          logger.fatal("Response body: #{response.body}")
        end
      end

      def generate_grafana_request(annotation_payload)
        request = Net::HTTP::Post.new(grafana_uri.path, { 'Content-Type' => 'application/json' })
        request['Authorization'] = "Bearer #{api_key}"
        request.body = annotation_payload

        request
      end

      def api_key
        @api_key ||= ENV.fetch('GRAFANA_API_KEY', nil)
      end

      def grafana_uri
        @grafana_uri ||= URI("#{ENV.fetch('GRAFANA_URL', nil)}/api/annotations")
      end
    end
  end
end
