# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class Artifact
        include ::SemanticLogger::Loggable

        FILE_NAME = 'PENDING_POST_DEPLOY_MIGRATIONS'
        # Fetches pending post-migrations sent by deployer and stores
        # them as an artifact. Deployer send  the information in the
        # following ways:
        #
        # - With pending post-migrations
        # =>"['20220510192117  foo', '20220523171107 bar']"
        #
        # - Without pending post-migrations
        # =>'[]'
        def create
          logger.info('Reading pending post migrations', raw_post_migrations: raw_pending_post_migrations)

          if pending_post_migrations.present?
            store_information(pending_post_migrations)
          else
            logger.warn('No pending post migrations')
          end
        end

        private

        def pending_post_migrations
          @pending_post_migrations ||= raw_pending_post_migrations.filter_map do |line|
            recast(line.strip)
          end.uniq
        end

        def raw_pending_post_migrations
          YAML.safe_load ENV.fetch('PENDING_MIGRATIONS', '')
        end

        def recast(raw_migration)
          raw_migration
            .gsub(/\s+/, '_')
            .downcase
            .concat('.rb')
        end

        def store_information(pending_post_migrations)
          logger.info('Storing post migrations', post_migrations: pending_post_migrations)

          File.write(FILE_NAME, pending_post_migrations.join(','))
        end
      end
    end
  end
end
