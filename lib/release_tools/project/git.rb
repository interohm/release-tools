# frozen_string_literal: true

module ReleaseTools
  module Project
    class Git < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/git.git',
        dev:       'git@dev.gitlab.org:gitlab/git.git',
        security:  'git@gitlab.com:gitlab-org/security/git.git'
      }.freeze
    end
  end
end
