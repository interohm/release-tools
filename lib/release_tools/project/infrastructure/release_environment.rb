# frozen_string_literal: true

module ReleaseTools
  module Project
    module Infrastructure
      class ReleaseEnvironment < Project::BaseProject
        REMOTES = {
          canonical: 'git@gitlab.com:gitlab-com/gl-infra/release-environments.git'
        }.freeze

        def self.default_branch
          'main'
        end
      end
    end
  end
end
