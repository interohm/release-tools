# frozen_string_literal: true

require 'state_machines/core'

module ReleaseTools
  module Metrics
    # Class used for modeling the states of the auto_deploy_environment_state metric
    # and making sure that a particular state change is valid.
    class EnvironmentStateTransition
      include ::SemanticLogger::Loggable
      extend StateMachines::MacroMethods

      METRIC = 'auto_deploy_environment_state'

      state_machine :current_state do
        event :deploy do
          transition %i[ready locked baking_time awaiting_promotion] => :locked
        end

        event :qa_success do
          transition [:locked] => :ready
        end

        event :gprd_cny_qa_success do
          transition [:locked] => :baking_time
        end

        event :baking_complete do
          # gprd-cny goes from baking_time state to ready state
          transition [:baking_time] => :ready

          # gstg and gprd go from ready state to awaiting_promotion
          transition [:ready] => :awaiting_promotion
        end
      end

      attr_reader :new_state, :current_state

      def initialize(environment, stage, new_state)
        @prometheus = ReleaseTools::Prometheus::Query.new
        @environment = environment
        @stage = stage
        @new_state = new_state

        super()
      end

      # Queries Prometheus to find the current value of `env_state` label of the metric and determine
      # if the label value can be changed to new_state.
      # Since Prometheus scrapes delivery-metrics every 15 seconds,
      # there is a 15 second delay in propogating metric changes to Prometeheus. This
      # 15 second delay could result in us retrieving a wrong value as the current
      # value of the metric from Prometheus.
      # We need to complete https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20083.
      # This issue will make Delivery-metrics a more reliable source of current
      # metric values.
      # Once this is complete, we can add a Get API to delivery-metrics
      # and call the delivery-metrics API to get the current value of the metric instead
      # of Prometheus.
      # @return [Boolean] true if the metric can be moved to new_state.
      def valid?
        @current_state = query_current_state
        return true if current_state.nil?

        # Check if new_state is in the list of valid states that
        # current_state can move to.
        current_state_transitions.collect(&:to).include?(new_state.to_s)
      end

      private

      attr_reader :client, :environment, :stage

      def query_current_state
        response = @prometheus.run(query)

        response.dig('data', 'result').each do |metric|
          next unless metric['value'][1] == '1'

          return metric['metric']['env_state']
        end

        nil
      end

      def query
        "#{METRIC}{target_env=\"#{environment}\",target_stage=\"#{stage}\"}"
      end
    end
  end
end
