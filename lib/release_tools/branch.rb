# frozen_string_literal: true

module ReleaseTools
  class Branch
    attr_reader :name, :project

    def initialize(name:, project:)
      @name = name
      @project = project
    end

    def create(ref:)
      Retriable.with_context(:api) do
        GitlabClient.create_branch(name, ref, project)
      end
    end

    def exist?
      Retriable.with_context(:api) do
        GitlabClient.find_branch(name, project)
      end
    end

    def last_commit
      Retriable.with_context(:api) do
        GitlabClient.commit(project, ref: name)
      end
    end

    def version
      return unless name.include?('stable')

      version_number = name
        .match(/\d+-\d+/)[0]
        .tr('-', '.')

      ReleaseTools::Version.new(version_number)
    end
  end
end
