# frozen_string_literal: true

namespace :deployments do
  desc 'Create a weekly deployment blockers report'
  task :blockers_report do
    issue = ReleaseTools::Deployments::BlockersReport.new

    create_or_show_report(issue)
  end

  desc 'Create a deployment blockers metrics'
  task :blockers_metrics do
    ReleaseTools::Deployments::BlockersMetrics.new.execute
  end

  desc 'Review all deployment blockers issues and create add Grafana annotations'
  task :blockers_annotate do
    ReleaseTools::Deployments::BlockerAnnotations.new.execute
  end
end
