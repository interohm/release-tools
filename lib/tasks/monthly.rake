# frozen_string_literal: true

namespace :monthly do
  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task :start do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :monthly)
        .execute
    end

    desc 'Update the protected stable branches'
    task :update_protected_branches do
      ReleaseTools::Monthly::Finalize::UpdateProtectedBranches.new.execute
    end

    desc 'Create a new monthly status metric'
    task :create_release_status_metric do
      ReleaseTools::Metrics::MonthlyReleaseStatus
      .new(status: :open)
      .execute
    end

    desc 'Create a new monthly version'
    task :create_version do
      ReleaseTools::Monthly::Finalize::CreateVersion.new.execute
    end
  end
end
