package consul

import (
	"strconv"

	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
)

type Endpoint struct {
	Host       string
	ConsulPort int
}

type ClientInterface interface {
	PutKeyValue(string, string) error
	GetKeyValue(string) ([]byte, error)
	DeleteKeyValue(string) error
}

type Client struct {
	client *api.Client
	logger *logrus.Logger
}

const namespace = "release-tools/"

// Creates a new Consul client
func CreateClient(endpoint Endpoint, logger *logrus.Logger) (ClientInterface, error) {
	config := api.DefaultConfig()
	config.Address = endpoint.Host + ":" + strconv.Itoa(endpoint.ConsulPort)

	client, err := api.NewClient(config)
	if err != nil {
		logger.WithError(err).Errorf("Failed to create Consul client")
		return nil, err
	}

	logger.WithField("address", config.Address).Info("Creating consul client")

	consulClient := Client{}
	consulClient.logger = logger
	consulClient.client = client

	return &consulClient, nil
}

// Adds a K/V pair to consul
func (c *Client) PutKeyValue(key, value string) error {
	pair := &api.KVPair{Key: c.getFullKey(key), Value: []byte(value)}

	kv := c.client.KV()
	_, err := kv.Put(pair, nil)

	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			WithField("value", value).
			Errorf("Failed to put key")

		return err
	}
	return nil
}

// Gets a value from consul
func (c *Client) GetKeyValue(key string) ([]byte, error) {
	kv := c.client.KV()

	pair, _, err := kv.Get(c.getFullKey(key), nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			Errorf("Failed to get key")

		return nil, err
	}

	return pair.Value, nil
}

// Deletes a value from consul
func (c *Client) DeleteKeyValue(key string) error {
	kv := c.client.KV()

	_, err := kv.Delete(c.getFullKey(key), nil)
	if err != nil {
		c.logger.
			WithError(err).
			WithField("key", key).
			Errorf("Failed to delete key")

		return err
	}

	return nil
}

func (c *Client) getFullKey(key string) string {
	return namespace + key
}
